﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructis.Logic
{
    public class Oficina : Apartamento
    {
        private int cantidadPuestos;
        private bool poseeEquipamiento;

        public int CantidadPuestos
        {
            get { return cantidadPuestos; }
            set { cantidadPuestos = value; }
        }

        public bool PoseeEquipamiento
        {
            get { return poseeEquipamiento; }
            set { poseeEquipamiento = value; }
        }

        /// <summary>
        /// Se setea el monto fijo para todas las oficinas
        /// </summary>
        public static double MontoFijo
        {
            get { return 2000; }
        }


        /// <summary>
        /// Se sobreescribe la función para adecuarla a los requerimientos del sistema
        /// </summary>
        /// <returns>double</returns>
        public override double CalcularPrecioVenta()
        {
            // Precio base * metraje total
            double calc = base.PrecioBase * base.MetrajeTotal;

            calc = calc + MontoFijo * cantidadPuestos; // Monto fijo por cada puesto se suma al total

            if (poseeEquipamiento)
            {
                // Si posee equipamiento se suma un 10% al totlal
                calc = calc + calc * 0.10;
            }

            return calc;
        }
    }
}
