﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructis.Logic
{
    public class Vendedor
    {
        #region Atributos
        private String clave;
        private String nombre;
        private List<Acuerdo> acuerdos = null;
        #endregion

        #region Propiedades
        public String Clave
        {
            get { return clave; }
            set { clave = value; }
        }

        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public List<Acuerdo> Acuerdos
        {
            get { return acuerdos; }
            set { acuerdos = value; }
        }
        #endregion

        #region Constructor
        public Vendedor()
        {
            acuerdos = new List<Acuerdo>();
        }
        #endregion

        #region Metodos

        public double CalcularComisionVenta(Venta venta)
        {
            double comision = 0;

            foreach (Acuerdo acuerdo in Acuerdos)
            {
                /* Se puede dar que el cliente compre apartamentos con el mismo numero de puerta en
                /  diferentes edificios, en ese caso no estoy seguro si el método Contains es eficiente
                /  @Matias
                */
                if (acuerdo.Edificio.Apartamentos.Contains(venta.Apartamento))
                {
                    comision = acuerdo.PorcentajeComision;
                }
            }

            return comision;
        }

        #endregion

    }
}
