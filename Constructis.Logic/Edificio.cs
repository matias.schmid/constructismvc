﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructis.Logic
{
    public class Edificio
    {
        #region "Atributos"

        private string nombre;
        private string direccion;
        private List<Apartamento> _apartamentos;

        #endregion

        #region Propiedades
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Direccion
        {
            get { return direccion; }
            set { direccion = value; }
        }

        public List<Apartamento> Apartamentos
        {
            get { return _apartamentos; }
        }
        #endregion

        #region Constructor

        public Edificio(string _nombre, string _direccion)
        {
            Nombre = _nombre;
            Direccion = _direccion;
            _apartamentos = new List<Apartamento>();
        }

        #endregion

        #region Metodos

        public bool AgregarApartamento(Apartamento _apartamento)
        {
            if (_apartamento != null)
            {
                Apartamentos.Add(_apartamento);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Apartamento ExisteApartamentoEnPiso(Apartamento _apartamentoABuscar)
        {
            Apartamento apartamentoEncontrado = null;
            foreach (Apartamento a in _apartamentos)
            {
                if (a.Piso == _apartamentoABuscar.Piso)
                {
                    if (a.Numero == _apartamentoABuscar.Numero || a.Orientacion == _apartamentoABuscar.Orientacion)
                    {
                        apartamentoEncontrado = a;
                    }
                }
            }
            return apartamentoEncontrado;
        }

        /* 
           Retorna true si el edificio al que se consulta posee al menos un apartamento cuyo rango
           de metraje se encuentre entre los parametros suministrados, adicionalmente chequea la
           existencia de un valor (no nulo) del tipo OrientacionApartamento, en cuyo caso se agrega
           a la expresion filtro.

           Nota: También podría resolverse con un método sobrecargado. 
        */
        public bool TieneApartamentosEnRangoDeMetraje(float minM2, float maxM2, OrientacionApartamento? orientacion)
        {
            bool aplica = false;
            foreach (Apartamento apto in Apartamentos)
            {
                if (orientacion.HasValue)
                {
                    if (apto.MetrajeTotal >= minM2 && apto.MetrajeTotal <= maxM2 && apto.Orientacion == orientacion)
                    {
                        aplica = true;
                    }
                }
                else
                {
                    if (apto.MetrajeTotal >= minM2 && apto.MetrajeTotal <= maxM2)
                    {
                        aplica = true;
                    }
                }
            }
            return aplica;
        }

        /// <summary>
        /// Lista los apartamentos aplicando un filtro de precios
        /// </summary>
        /// <param name="precioMin">double</param>
        /// <param name="precioMax">double</param>
        /// <returns>List<Apartamentos></returns>
        public List<Apartamento> ObtenerApartamentosEnRangoDePrecio(double precioMin, double precioMax)
        {
            List<Apartamento> apartamentosQueCumplen = new List<Apartamento>();
            foreach (Apartamento apto in Apartamentos)
            {
                if (apto.PrecioVenta >= precioMin && apto.PrecioVenta <= precioMax)
                {
                    apartamentosQueCumplen.Add(apto);
                }
            }
            return apartamentosQueCumplen;
        }

        
        public List<Apartamento> ObtenerApartamentosParaVender()
        {
            List<Apartamento> apartamentosVender = new List<Apartamento>();

            foreach(Apartamento ap in Apartamentos){
                if (!ap.Vendido)
                {
                    apartamentosVender.Add(ap);
                }
            }

            return apartamentosVender;
        }

        
        /// <summary>
        /// Se sobreescribe la funcion ToString
        /// </summary>
        /// <returns>String</returns>
        public override string ToString()
        {
            return "Nombre: " + Nombre + " - Direccion: " + Direccion;
        }
        #endregion

    }
}
