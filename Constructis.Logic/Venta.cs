﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructis.Logic
{
    public class Venta
    {
        #region Atributos
        private Cliente cliente;
        private Vendedor vendedor;
        private Apartamento apartamento;
        private DateTime fechaVenta;
        #endregion

        #region Propiedades
        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        public Vendedor Vendedor
        {
            get { return vendedor; }
            set { vendedor = value; }
        }

        public Apartamento Apartamento
        {
            get { return apartamento; }
            set { apartamento = value; }
        }

        public DateTime FechaVenta
        {
            get { return fechaVenta; }
            set { fechaVenta = value; }
        }
        #endregion

    }
}
