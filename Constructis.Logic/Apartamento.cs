﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructis.Logic
{
    public abstract class Apartamento:IEquatable<Apartamento>
    {
        #region Atributos
        protected int piso;
        protected int numero;
        protected float metrajeTotal;
        protected double precioBase;
        protected OrientacionApartamento orientacion;
        protected Boolean vendido;
        #endregion

        #region Propiedades
        public int Piso
        {
            get { return piso; }
            set { piso = value; }
        }

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        public float MetrajeTotal
        {
            get { return metrajeTotal; }
            set { metrajeTotal = value; }
        }

        public double PrecioBase
        {
            get { return precioBase; }
            set { precioBase = value; }
        }

        public double PrecioVenta
        {
            get { return CalcularPrecioVenta(); }
        }

        public OrientacionApartamento Orientacion
        {
            get { return orientacion; }
            set { orientacion = value; }
        }

        public Boolean Vendido
        {
            get { return vendido; }
            set { vendido = value; }
        }
        #endregion

        #region Metodos

        /// <summary>
        /// Por temas de responsabilidades se declara la función aqui, luego se sobreescribirá en las clases que hereden de esta.
        /// </summary>
        /// <returns>double</returns>
        public abstract double CalcularPrecioVenta();

        /// <summary>
        /// Se implementa el método Equals - chequeando por Numero de puerta, 
        /// </summary>
        /// <returns>string</returns>
        public bool Equals(Apartamento otroApartamento)
        {
            return Piso == otroApartamento.Piso &&
            Numero == otroApartamento.Numero &&
            MetrajeTotal == otroApartamento.MetrajeTotal &&
            PrecioBase == otroApartamento.PrecioBase && 
            Orientacion == otroApartamento.Orientacion;
        }

        /// <summary>
        /// Se sobreescribe la función ToString
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            return "Piso: " + Piso + " - Número: " + Numero + " - Precio: " + PrecioVenta;
        }
        #endregion

    }
}
