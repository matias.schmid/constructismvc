﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructis.Logic
{
    public class Constructora
    {
        #region Atributos
        private static Constructora _instancia = null;
        private List<Edificio> _edificios = null;
        private List<Vendedor> _vendedores = null;
        private List<Cliente> _clientes = null;
        private List<Venta> _ventas = null;

        #endregion

        #region Propiedades
        /// <summary>
        /// Instancia, Patron Singleton
        /// </summary>
        /// <returns>Constructora</returns>
        public static Constructora ObtenerInstancia
        {
            get
            {
                if (_instancia == null)
                {
                    _instancia = new Constructora();
                }
                return _instancia;
            }
        }

        public List<Edificio> Edificios
        {
            get { return _edificios; }
        }

        public List<Vendedor> Vendedores
        {
            get { return _vendedores; }
        }

        public List<Cliente> Clientes
        {
            get { return _clientes; }
        }

        public List<Venta> Ventas
        {
            get { return _ventas; }
        }

        #endregion

        #region Constructor Privado
        /// <summary>
        /// Constructor de la Clase
        /// </summary>
        private Constructora()
        {
            _vendedores = new List<Vendedor>(); // Falta en el UML
            _clientes = new List<Cliente>();
            _ventas = new List<Venta>();
            _edificios = new List<Edificio>();
            PrecargarDatos();
        }
        #endregion

        /// <summary>
        /// PrecargarDatos
        /// Método para precargar datos de prueba
        /// </summary>
        private void PrecargarDatos()
        {
            // Precargar Cliente
            Cliente cli1 = new Cliente()
            {
                Nombre = "Matias",
                Apellido = "Schmid",
                Documento = "47707409",
                Direccion = "Juan Pablo Laguna 3329",
                Telefono = "094593160"
            };

            Cliente cli2 = new Cliente()
            {
                Nombre = "Ignacio",
                Apellido = "Cabrera",
                Documento = "47329670",
                Direccion = "Av.Italia 2652",
                Telefono = "092041396"
            };

            Cliente cli3 = new Cliente()
            {
                Nombre = "Luis",
                Apellido = "Dentone",
                Documento = "24449872",
                Direccion = "Maldonado 2241",
                Telefono = "099113003"
            };

            Clientes.Add(cli1);
            Clientes.Add(cli2);
            Clientes.Add(cli3);

            // Precarga de Vendedor
            Vendedor Vend1 = new Vendedor()
            {
                Nombre = "vend1",
                Clave = "vend1111"
            };

            Vendedor Vend2 = new Vendedor()
            {
                Nombre = "vend2",
                Clave = "vend2222"
            };

            Vendedores.Add(Vend1);
            Vendedores.Add(Vend2);

            // Precarga de apartamentos
            Apartamento of1220 = new Oficina()
            {
                CantidadPuestos = 34,
                MetrajeTotal = 120,
                Numero = 1220,
                Orientacion = OrientacionApartamento.N,
                Piso = 12,
                PoseeEquipamiento = true,
                PrecioBase = 120.000,
                Vendido = false
            };

            Apartamento ap301 = new Casa_habitacion()
            {
                MetrajeTotal = 120,
                Numero = 301,
                Orientacion = OrientacionApartamento.N,
                Piso = 3,
                CantidadDormitorios = 1,
                CantidadWC = 1,
                TieneGarage = true,
                PrecioBase = 120.000,
                Vendido = true
            };

            Apartamento ap603 = new Casa_habitacion()
            {
                MetrajeTotal = 100,
                Numero = 603,
                Orientacion = OrientacionApartamento.NO,
                Piso = 6,
                CantidadDormitorios = 1,
                CantidadWC = 1,
                TieneGarage = false,
                PrecioBase = 105.000,
                Vendido = false
            };

            Apartamento ap604 = new Casa_habitacion()
            {
                MetrajeTotal = 100,
                Numero = 604,
                Orientacion = OrientacionApartamento.NO,
                Piso = 6,
                CantidadDormitorios = 1,
                CantidadWC = 1,
                TieneGarage = false,
                PrecioBase = 105.000,
                Vendido = false
            };

            Apartamento ap605 = new Casa_habitacion()
            {
                MetrajeTotal = 100,
                Numero = 605,
                Orientacion = OrientacionApartamento.NO,
                Piso = 6,
                CantidadDormitorios = 1,
                CantidadWC = 1,
                TieneGarage = false,
                PrecioBase = 105.000,
                Vendido = false
            };

            Edificio diamantis = new Edificio("Diamantis Plaza", "Av.Rivera 1234");
            diamantis.AgregarApartamento(of1220);
            diamantis.AgregarApartamento(ap605);

            Edificio lagunaSur = new Edificio("Laguna Sur", "Juan Pablo Laguna 3329");
            lagunaSur.AgregarApartamento(ap301);

            Edificio plazaCentro = new Edificio("Plaza Centro", "Rio Branco 1407 esq. Colonia");
            plazaCentro.AgregarApartamento(ap603);
            plazaCentro.AgregarApartamento(ap604);

            Edificios.Add(diamantis);
            Edificios.Add(lagunaSur);
            Edificios.Add(plazaCentro);

            // Precargar acuerdos (comisiones)
            Acuerdo ACDiamantis = new Acuerdo()
            {
                PorcentajeComision = 20,
                Edificio = diamantis
            };

            Acuerdo ACLagunaSur = new Acuerdo()
            {
                PorcentajeComision = 35,
                Edificio = lagunaSur
            };

            Acuerdo ACPlazaCentro = new Acuerdo()
            {
                PorcentajeComision = 155,
                Edificio = plazaCentro
            };

            Vend1.Acuerdos.Add(ACDiamantis);
            Vend2.Acuerdos.Add(ACLagunaSur);
            Vend1.Acuerdos.Add(ACPlazaCentro);

            // Precargar Venta
            Venta venta1 = new Venta()
            {
                Apartamento = ap301,
                Cliente = cli1,
                FechaVenta = new DateTime(2018, 1, 31),
                Vendedor = Vend2
            };

            Venta venta2 = new Venta()
            {
                Apartamento = of1220,
                Cliente = cli1,
                FechaVenta = new DateTime(2018, 5, 1),
                Vendedor = Vend2
            };

            Venta venta3 = new Venta()
            {
                Apartamento = ap603,
                Cliente = cli2,
                FechaVenta = new DateTime(2017, 8, 15),
                Vendedor = Vend1
            };

            Venta venta4 = new Venta()
            {
                Apartamento = ap604,
                Cliente = cli3,
                FechaVenta = new DateTime(2015, 2, 2),
                Vendedor = Vend1
            };

            Ventas.Add(venta1);
            Ventas.Add(venta2);
            Ventas.Add(venta3);
            Ventas.Add(venta4);

        }

        #region Metodos Edificio

        /// <summary>
        /// Busca un edificio por el nombre en la lista de edificios
        /// </summary>
        /// <param name="_nombre">String, nombre del edificio</param>
        /// <returns>Edificio</returns>
        public Edificio BuscarEdificio(string _nombre)
        {
            Edificio edificioRetorno = null;
            List<Edificio> edificios = Edificios;
            foreach (Edificio e in edificios)
            {
                if (e.Nombre == _nombre)
                {
                    edificioRetorno = e;
                }
            }
            return edificioRetorno;
        }

        /// <summary>
        /// Lista Edificios con la aplicación de un filtro
        /// </summary>
        /// <param name="minMetros"></param>
        /// <param name="maxMetros"></param>
        /// <param name="orientacionA"></param>
        /// <returns>List<Edificio></returns>
        public List<Edificio> ListarEdificiosFiltrados(float minMetros, float maxMetros, OrientacionApartamento? orientacionA)
        {
            List<Edificio> edificiosFiltrados = new List<Edificio>();

            foreach (Edificio e in Edificios)
            {
                if (orientacionA.HasValue)
                {
                    if (e.TieneApartamentosEnRangoDeMetraje(minMetros, maxMetros, orientacionA))
                    {
                        edificiosFiltrados.Add(e);
                    }
                }
                else
                {
                    if (e.TieneApartamentosEnRangoDeMetraje(minMetros, maxMetros, null))
                    {
                        edificiosFiltrados.Add(e);
                    }
                }
            }
            return edificiosFiltrados;
        }

        public Apartamento BuscarApartamentoEnEdificio(String edificio, int piso, int numero)
        {
            Apartamento apto = null;
            Edificio ed = BuscarEdificio(edificio);

            int i = 0;
            Boolean encontrado = false;
            while (i < ed.Apartamentos.Count && ! encontrado)
            {
                if (ed.Apartamentos[i].Piso == piso && ed.Apartamentos[i].Numero == numero)
                {
                    apto = ed.Apartamentos[i];
                    encontrado = true;
                }
                i++;
            }

            return apto;
        }
        #endregion

        #region Metodos Vendedor
        /// <summary>
        /// Chequea la existencia de un vendedor especifico en el sistema
        /// </summary>
        /// <param name="nombreUsuario"></param>
        /// <param name="clave"></param>
        /// <returns>Vendedor</returns>
        /// 

        public Vendedor BuscarVendedor(string nombreUsuario, string clave)
        {
            Vendedor vendedorEncontrado = null;
            List<Vendedor> vendedores = Vendedores;
            foreach (Vendedor v in vendedores)
            {
                if (!string.IsNullOrEmpty(nombreUsuario) && !string.IsNullOrEmpty(clave))
                {
                    if (v.Nombre == nombreUsuario && v.Clave == clave)
                    {
                        vendedorEncontrado = v;
                    }
                }
                else if (!string.IsNullOrEmpty(nombreUsuario))
                {
                    if (v.Nombre == nombreUsuario)
                    {
                        vendedorEncontrado = v;
                    }
                }
            }
            return vendedorEncontrado;
        }

        /// <summary>
        /// Listar todas las ventas realizadas por el vendedor logueado
        /// </summary>
        /// <param name="nombreUsuario"></param>
        /// <returns>Lista de ventas</returns>
        /// 
        public List<Venta> ObtenerVentasDeVendedor(string nombreUsuario)
        {
            List<Venta> ventasRetorno = new List<Venta>();

            // Buscamos un vendedor por su nombre de usuario, se asume que no deben existir dos vendedores con el mismo nombre de usuario.
            if (BuscarVendedor(nombreUsuario, null) != null)
            {
                foreach (Venta v in Ventas)
                {
                    if (v.Vendedor.Nombre == nombreUsuario)
                    {
                        ventasRetorno.Add(v);
                    }
                }
            }
            return ventasRetorno;
        }

        public Vendedor BuscarVendedor(String nombre)
        {
            int i = 0;
            Boolean encontrado = false;
            Vendedor ven = null;

            while (i < Vendedores.Count && !encontrado)
            {
                if (Vendedores[i].Nombre == nombre)
                {
                    ven = Vendedores[i];
                    encontrado = true;
                }
                i++;
            }

            return ven;

        }
        #endregion

        #region Agregar
        /// <summary>
        /// Agrega Apartamentos al Edificio y Agrega el Edificio a la lista de edificios en Constructora
        /// </summary>
        /// <param name="unEdificio">Edificio</param>
        /// <param name="unApartamento">Apartamento</param>
        public void AgregarEntidades(Edificio unEdificio, Apartamento unApartamento)
        {
            // Si los parametros son nulos, entonces no hay nada para agregar.
            if (unEdificio != null && unApartamento != null)
            {
                // Chequeamos la existencia de un edificio con el nombre que nos suministran.
                if (BuscarEdificio(unEdificio.Nombre) == null)
                {
                    /* Si no existe apartamento con el mismo número u orientación en el mismo piso, 
                     * entonces podemos agregar el apartamento al edificio.*/
                    if (unEdificio.ExisteApartamentoEnPiso(unApartamento) == null)
                    {
                        unEdificio.AgregarApartamento(unApartamento);
                        Edificios.Add(unEdificio);
                    }
                }
            }
        }

        #endregion

        #region Metodos Apartamento
        /// <summary>
        /// Lista Apartamentos aplicando un filtro, Precio Mínimo y Pecio Máximo
        /// </summary>
        /// <param name="precioMin">double</param>
        /// <param name="precioMax">double</param>
        /// <returns>List<Apartamento></returns>
        public List<Apartamento> ObtenerApartamentosPorRangoPrecio(double precioMin, double precioMax)
        {
            List<Apartamento> apartamentos = new List<Apartamento>();
            foreach (Edificio unEdificio in Edificios)
            {
                List<Apartamento> apartamentosPorEdificio = unEdificio.ObtenerApartamentosEnRangoDePrecio(precioMin, precioMax);
                if (apartamentosPorEdificio.Count > 0)
                {
                    apartamentos.AddRange(apartamentosPorEdificio);
                }
            }
            return apartamentos;
        }

        /// <summary>
        /// Cuenta la cantidad de apartamentos filtrados por su metraje
        /// </summary>
        /// <param name="metrajeMin">float</param>
        /// <param name="metrajeMax">float</param>
        /// <returns>int</returns>
        public int CantidadApartamentosPorRangoMetraje(float metrajeMin, float metrajeMax)
        {
            int cantidad = 0;

            foreach (Edificio unEdificio in Edificios)
            {
                if (unEdificio.TieneApartamentosEnRangoDeMetraje(metrajeMin, metrajeMax, null))
                {
                    cantidad += 1;
                }
            }
            return cantidad;
        }

        #endregion

        #region Metodos Cliente
        /// <summary>
        /// Retorna una lista de Clientes que hayan comprado al menos un apartamento entre un rango de fechas
        /// </summary>
        /// <param name="fechaDesde">DateTime</param>
        /// <param name="fechaHasta">DateTime</param>
        /// <returns>List<Cliente></returns>
        public List<Cliente> ObtenerListaClientesConCompras(DateTime fechaDesde, DateTime fechaHasta)
        {
            List<Cliente> clientesRetorno = new List<Cliente>();
            foreach (Venta v in Ventas)
            {
                if (v.FechaVenta >= fechaDesde && v.FechaVenta <= fechaHasta)
                {
                    clientesRetorno.Add(v.Cliente);
                }
            }
            clientesRetorno.Sort();
            return clientesRetorno;
        }

        public Boolean AgregarCliente(String nombre, String apellido, String documento, String direccion, String telefono)
        {
            Boolean correcto = false;

            if (nombre != "" && apellido != "" && documento != "" && direccion != "" && telefono != "")
            {
                Cliente c = new Cliente()
                {
                    Nombre = nombre,
                    Apellido = apellido,
                    Documento = documento,
                    Direccion = direccion,
                    Telefono = telefono
                };

                _clientes.Add(c);
                correcto = true;

            }

            return correcto;
        }

        public Cliente BuscarCliente(String documento)
        {
            Cliente cli = null;
            Boolean encontrado = false;
            int i = 0;

            while (encontrado == false && i < Clientes.Count)
            {

                if (documento == Clientes[i].Documento)
                {
                    cli = Clientes[i];
                    encontrado = true;
                }
                else
                {
                    i++;
                }
            }

            return cli;
        }

        public Boolean EliminarCliente(String documento)
        {
            Boolean eliminado = false;

            int i = 0;
            while (!eliminado && i < Clientes.Count)
            {
                if (Clientes[i].Documento == documento)
                {

                    int x = 0;
                    Boolean canEliminar = true;

                    while (x < Ventas.Count && canEliminar)
                    {
                        if (Ventas[x].Cliente.Documento == Clientes[i].Documento)
                        {
                            canEliminar = false;
                        }
                        else
                        {
                            x++;
                        }
                    }

                    if (canEliminar)
                    {
                        Clientes.RemoveAt(i);
                        eliminado = true;
                    }
                }

                i++;

            }

            return eliminado;
        }
        #endregion

    }
}
