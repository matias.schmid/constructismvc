﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructis.Logic
{
    public class Casa_habitacion : Apartamento
    {
        private int cantidadDormitorios;
        private int cantidadWC;
        private bool tieneGarage; // Si es true es porque lo tiene

        public int CantidadDormitorios
        {
            get { return cantidadDormitorios; }
            set { cantidadDormitorios = value; }
        }

        public int CantidadWC
        {
            get { return cantidadWC; }
            set { cantidadWC = value; }
        }

        public bool TieneGarage
        {
            get { return tieneGarage; }
            set { tieneGarage = value; }
        }

        /// <summary>
        /// Se setea el mont fijo para todas las casas
        /// </summary>
        public static double MontoFijo
        {
            get { return 2000; }
        }


        /// <summary>
        /// Se sobreescribe la función para adecuarla a los requerimientos del sistema
        /// Calcula el precio de venta de cada casa
        /// </summary>
        /// <returns>double</returns>
        public override double CalcularPrecioVenta()
        {
            // Precio base * metraje total
            double calc = base.PrecioBase * base.MetrajeTotal;

            if (cantidadDormitorios > 0 && cantidadDormitorios < 3)
            {
                // Cantidad de dormitorios entre 1 y 2
                calc = calc + calc * 0.05; // Precio base + el 5%

            } else if (cantidadDormitorios < 5)
            {
                // Cantidad de dormitorios entre 3 y 4
                calc = calc + calc * 0.10; // Precio base + el 10%

            } else if (cantidadDormitorios > 4)
            {
                // Cantidad de dormitorios 4 y mas
                calc = calc + calc * 0.20; // Precio base + el 20%
            }

            if (tieneGarage)
            {
                // Si tiene garage se suma un monto fijo
                calc = calc + MontoFijo;
            }

            if (Orientacion == OrientacionApartamento.N || Orientacion == OrientacionApartamento.NE || Orientacion == OrientacionApartamento.NO)
            {
                // Si la orientacion es N, NE, NO se suma un 15%
                calc = calc + calc * 0.15; // Precio calculado + el 15%
            }

            return calc;
        }
    }
}
