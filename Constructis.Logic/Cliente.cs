﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructis.Logic
{
    public class Cliente:IComparable<Cliente>
    {
        private string nombre;
        private string apellido;
        private string documento;
        private string direccion;
        private string telefono;


        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public String Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
        public String Documento
        {
            get { return documento; }
            set { documento = value; }
        }
        public String Direccion
        {
            get { return direccion; }
            set { direccion = value; }
        }
        public String Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public String NombreCompleto
        {
            get
            {
                return String.Format("{0} {1}", this.nombre, this.apellido);
            }
        }

        public int CompareTo(Cliente otroCliente)
        {
            return this.Nombre.CompareTo(otroCliente.Nombre);
        }
    }
}
