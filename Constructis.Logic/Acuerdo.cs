﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructis.Logic
{
    public class Acuerdo
    {
        public double PorcentajeComision { get; set; }
        public Edificio Edificio { get; set; }
    }
}
