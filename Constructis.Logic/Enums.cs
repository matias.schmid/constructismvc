﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructis.Logic
{
    public enum OrientacionApartamento
    {
        N,
        NE,
        E,
        SE,
        S,
        SO,
        O,
        NO
    }
}
