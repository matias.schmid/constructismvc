﻿using Constructis.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConstructisV2.web.Controllers
{
    public class ClienteController : Controller
    {
        // GET: Cliente/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Cliente/Create
        public ActionResult Registro()
        {
            return View();
        }

        // POST: Cliente/Create
        [HttpPost]
        public ActionResult Registro(FormCollection collection)
        {
            Boolean agregado = false;
            String msj = "Ocurrió un error al registrar el cliente";
            String nombreClaseAlerta = "alert alert-danger";
            if (collection["txtNombre"] != "" && collection["txtApellido"] != "" && collection["txtDocumento"] != "" && collection["txtDireccion"] != "" && collection["txtTelefono"] != "")
            {
                Constructora constructora = Constructora.ObtenerInstancia;

                if (constructora.BuscarCliente(collection["txtDocumento"]) == null)
                {
                    agregado = constructora.AgregarCliente(collection["txtNombre"], collection["txtApellido"], collection["txtDocumento"], collection["txtDireccion"], collection["txtTelefono"]);
                    nombreClaseAlerta = "alert alert-success";
                    msj = "El cliente ha sido registrado con éxito";
                }
            }

            ViewBag.ClaseAlerta = nombreClaseAlerta;
            ViewBag.Mensaje = msj;

            return View();
        }

        // GET: Cliente/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Cliente/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Cliente/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Cliente/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
