﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Constructis.Logic;
using ConstructisV2.web.Models;

namespace ConstructisV2.web.Controllers
{
    public class VendedorController : Controller
    {
        // GET: Usuario
        public ActionResult Index()
        {
            return View();
        }

        // GET: Obtener lista de clientes compradores en un rango de fechas
        public ActionResult ListaCompradores()
        {
            if (!AutorizadorAcceso.Autorizado("vendedor"))
            {
                return RedirectToAction("Index", "Vendedor");
            }
            else
            {
                ViewBag.errorCamposVacios = "";
                return View();
            }
        }

        [HttpGet]
        public ActionResult ObtenerApartamentos(string nombreEdificio)
        {
            if (!string.IsNullOrWhiteSpace(nombreEdificio))
            {
                Edificio edificio = Constructora.ObtenerInstancia.BuscarEdificio(nombreEdificio);
                IEnumerable<Apartamento> aptos = edificio.Apartamentos;
                return Json(aptos, JsonRequestBehavior.AllowGet);
            }
            return null;
        }



        [HttpPost]
        public ActionResult ListaCompradores(DateTime? fechaDesde, DateTime? fechaHasta)
        {
            List<Cliente> compradores = null;
            if (fechaDesde.HasValue && fechaHasta.HasValue)
            {
                ViewBag.FechaDesde = fechaDesde.Value.ToString("dd/MM/yyyy");
                ViewBag.FechaHasta = fechaHasta.Value.ToString("dd/MM/yyyy");
                compradores = Constructora.ObtenerInstancia.ObtenerListaClientesConCompras(fechaDesde.Value, fechaHasta.Value);
            }
            else
            {
                ViewBag.errorCamposVacios = "Los campos de fecha son obligatorios";
            }
            return View(compradores);
        }


        // GET: Obtener lista de ventas del vendedor logueado
        public ActionResult ListaVentas()
        {
            if (!AutorizadorAcceso.Autorizado("vendedor"))
            {
                return RedirectToAction("Index", "Vendedor");
            }

            List<Venta> ventasDeVendedor = Constructora.ObtenerInstancia.ObtenerVentasDeVendedor(Session["nombre"] as string);
            List<VentaViewModel> ventasVMs = new List<VentaViewModel>();
            if (ventasDeVendedor.Count() > 0)
            {
                foreach (Venta v in ventasDeVendedor)
                {
                    VentaViewModel vm = new VentaViewModel();
                    Vendedor vendedor = v.Vendedor;
                    vm.FechaVenta = v.FechaVenta;
                    vm.Apartamento = v.Apartamento;
                    vm.Cliente = v.Cliente;
                    vm.ComisionDeVenta = vendedor.CalcularComisionVenta(v);
                    ventasVMs.Add(vm);
                }
            }

            return View(ventasVMs);
        }

        [HttpPost]
        public ActionResult Index(string txtUsuario, string txtClave)
        {
            if (txtUsuario != "" && txtClave != "")
            {
                if (Constructora.ObtenerInstancia.BuscarVendedor(txtUsuario, txtClave) != null)
                {
                    Session["nombre"] = txtUsuario;
                    Session["rol"] = "vendedor";
                    return RedirectToAction("Index", "Home");
                }
            }

            Session["nombre"] = null;
            @ViewBag.Mensaje = "Usuario no encontrado";
            return View();
        }

        public ActionResult CerrarSesion()
        {
            Session["nombre"] = null;
            Session["rol"] = null;
            return RedirectToAction("Index");
        }

        public ActionResult ListaClientes(String msj)
        {
            if (!AutorizadorAcceso.Autorizado("vendedor"))
            {
                return RedirectToAction("Index", "Vendedor");
            }

            // Instancia de Constructora
            Constructora constructora = Constructora.ObtenerInstancia;

            List<Cliente> clientes = constructora.Clientes;
            ViewBag.Mensaje = msj;

            return View(clientes);
        }

        public ActionResult EliminarCliente(String documento)
        {
            Constructora constructora = Constructora.ObtenerInstancia;
            Boolean eliminado = constructora.EliminarCliente(documento);
            //String msj;

            //if (eliminado)
            //{
            //    msj = "Se ha eliminado el cliente";
            //} else
            //{
            //    msj = "No se ha podido eliminar el cliente";
            //}

            return RedirectToAction("ListaClientes");
        }

        //GET: Editar Cliente
        public ActionResult EditarCliente(String documento)
        {
            if (!AutorizadorAcceso.Autorizado("vendedor"))
            {
                return RedirectToAction("Index", "Vendedor");
            }

            if (documento == null || documento == "")
            {
                return Redirect("ListaClientes");
            }

            Constructora constructora = Constructora.ObtenerInstancia;

            Cliente c = constructora.BuscarCliente(documento);

            ViewBag.Documento = c.Documento;
            ViewBag.Nombre = c.Nombre;
            ViewBag.Apellido = c.Apellido;
            ViewBag.Direccion = c.Direccion;
            ViewBag.Telefono = c.Telefono;


            return View("EditarCliente");
        }

        [HttpPost]
        public ActionResult EditarCliente(FormCollection collection)
        {

            String msj = "No se ha podido editar, revise los campos";
            String nombreClaseAlerta = "alert alert-danger";


            if (collection["txtNombre"] != "" && collection["txtApellido"] != "" && collection["txtDocumento"] != "" && collection["txtDireccion"] != "" && collection["txtTelefono"] != "")
            {

                Constructora constructora = Constructora.ObtenerInstancia;
                String doc = collection["txtDocumento"];
                Cliente c = constructora.BuscarCliente(doc);

                c.Nombre = collection["txtNombre"];
                c.Apellido = collection["txtApellido"];
                c.Direccion = collection["txtDireccion"];
                c.Telefono = collection["txtTelefono"];

                nombreClaseAlerta = "alert alert-success";
                msj = "Se ha editado correctamente";
            }

            ViewBag.ClaseAlerta = nombreClaseAlerta;
            ViewBag.Mensaje = msj;


            return View("EditarCliente");
        }
    }
}