﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Constructis.Logic;

namespace ConstructisV2.web.Controllers
{
    public class VentaController : Controller
    {
        // GET: Venta
        public ActionResult Vender()
        {
            if (!AutorizadorAcceso.Autorizado("vendedor"))
            {
                return RedirectToAction("Index", "Vendedor");
            }

            // Instancia de constructora
            Constructora constructora = Constructora.ObtenerInstancia;

            // Lista de Clientes
            ViewBag.Clientes = constructora.Clientes;

            // Lista de Edificios
            ViewBag.Edificios = constructora.Edificios;

            return View("Vender");
        }


        // Post Vender
        [HttpPost]
        public ActionResult Vender(FormCollection collection)
        {

            if (!AutorizadorAcceso.Autorizado("vendedor"))
            {
                return RedirectToAction("Index", "Vendedor");
            }

            String cliente = collection["cmbCliente"];
            String edificio = collection["cmbEdificio"];

            return RedirectToAction("FinalizadoVenta", new { cliente = cliente, edificio = edificio });
        }


        public ActionResult FinalizadoVenta(String cliente, String edificio)
        {
            if (!AutorizadorAcceso.Autorizado("vendedor"))
            {
                return RedirectToAction("Index", "Vendedor");
            }

            Constructora constructora = Constructora.ObtenerInstancia;
            ViewBag.Cliente = cliente;
            ViewBag.Edificio = edificio;
            ViewBag.Apartamentos = constructora.BuscarEdificio(edificio).ObtenerApartamentosParaVender();

            return View();
        }

        [HttpPost]
        public ActionResult FinalizadoVenta(FormCollection collection)
        {
            if (!AutorizadorAcceso.Autorizado("vendedor"))
            {
                return RedirectToAction("Index", "Vendedor");
            }

            // Instancia de constructora
            Constructora constructora = Constructora.ObtenerInstancia;
            String msj = "No se ha podido vender, revise los campos del formulario";
            String cls = "alert-danger";

            if (collection["txtCliente"] != "" && collection["txtEdificio"] != "" && collection["cmbApartamento"] != "")
            {
                String edificio = collection["txtEdificio"];
                int piso = int.Parse(collection["cmbApartamento"].Split(':')[1].Split(' ')[1]);
                int numero = int.Parse(collection["cmbApartamento"].Split(':')[2].Split(' ')[1]);
                String vendedor = Session["nombre"] as String;

                Venta venta = new Venta()
                {
                    Cliente = constructora.BuscarCliente(collection["txtCliente"]),
                    Vendedor = constructora.BuscarVendedor(vendedor),
                    Apartamento = constructora.BuscarApartamentoEnEdificio(edificio, piso, numero),
                    FechaVenta = DateTime.Today

                };

                // Se setea en true la variable vendido para que no se pueda vender mas
                constructora.BuscarApartamentoEnEdificio(edificio, piso, numero).Vendido = true;

                // Se agrega la venta a la lista de ventas
                constructora.Ventas.Add(venta);

                msj = "Se ha vendido el apartamento";
                cls = "alert-success";
            }

            ViewBag.Vendido = msj;
            ViewBag.Clase = cls;

            return View();
        }

    }
}