﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConstructisV2.web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (!AutorizadorAcceso.Autorizado("vendedor"))
            {
                return RedirectToAction("Index", "Vendedor");
            }
            else
            {
                return View();
            }
        }
    }
}