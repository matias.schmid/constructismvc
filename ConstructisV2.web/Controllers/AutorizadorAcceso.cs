﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstructisV2.web.Controllers
{
    public static class AutorizadorAcceso
    {
        public static bool Autorizado(string rol)
        {
            string rolLogeado = HttpContext.Current.Session["rol"] as string;
            return rolLogeado == rol;
        }
    }
}