﻿using Constructis.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstructisV2.web.Models
{
    public class VentaViewModel
    {
        public DateTime FechaVenta { get; set; }
        public Apartamento Apartamento { get; set; }
        public Cliente Cliente { get; set; }
        public double ComisionDeVenta { get; set; }

        #region ToString


        #endregion
    }
}