﻿using Constructis.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ConstructisV2.web.Models
{
    public class FormVentaViewModel
    {
        [Required]
        [Display(Name = "Cliente")]
        public string DocumentoCliente { get; set; }
        public IEnumerable<Cliente> Clientes { get; set; }

        [Required]
        [Display(Name = "Edificio")]
        public string Direccion { get; set; }
        public IEnumerable<Edificio> Edificios { get; set; }

        [Required]
        [Display(Name = "Apartamento")]
        public double PrecioBase { get; set; }
        public IEnumerable<Apartamento> Apartamentos { get; set; }

        public FormVentaViewModel()
        {
            this.Clientes = Constructora.ObtenerInstancia.Clientes;
            this.Edificios = Constructora.ObtenerInstancia.Edificios;
            this.Apartamentos = new List<Apartamento>();
        }
    }
}